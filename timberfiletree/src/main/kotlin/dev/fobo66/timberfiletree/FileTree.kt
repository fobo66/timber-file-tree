package dev.fobo66.timberfiletree

import timber.log.Timber
import java.io.File

class FileTree(
    logDirectory: File,
    logFileName: String,
) : Timber.Tree() {
    private val logFile = File(logDirectory, logFileName)

    init {
        if (!logFile.exists()) {
            logFile.createNewFile()
        }
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        logFile.appendText(message + System.lineSeparator())
    }
}
