package dev.fobo66.timberfiletree

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import timber.log.Timber
import java.io.File
import java.util.concurrent.CountDownLatch

private const val LOG_FILE_NAME = "test.log"

class FileTreeTest {

    @get:Rule
    val tempDirRule = TemporaryFolder()

    @Test
    fun `create log file`() {
        val tempDir = tempDirRule.newFolder()
        val tree = FileTree(tempDir, LOG_FILE_NAME)
        Timber.plant(tree)
        val logFile = File(tempDir, LOG_FILE_NAME)
        assertTrue(logFile.exists())
        Timber.uproot(tree)
    }

    @Test
    fun `log text`() {
        val tempDir = tempDirRule.newFolder()
        val tree = FileTree(tempDir, LOG_FILE_NAME)
        Timber.plant(tree)
        Timber.d("Test")
        val logFile = File(tempDir, LOG_FILE_NAME)
        val content = logFile.readText()
        assertEquals("Test\n", content)
        Timber.uproot(tree)
    }

    @Test
    fun `log text from multiple threads`() {
        val tempDir = tempDirRule.newFolder()
        val tree = FileTree(tempDir, LOG_FILE_NAME)
        val threadLatch = CountDownLatch(2)
        val firstThread = Thread {
            Timber.d("Test 1")
            threadLatch.countDown()
        }
        val secondThread = Thread {
            Timber.d("Test 2")
            threadLatch.countDown()
        }

        Timber.plant(tree)
        firstThread.start()
        secondThread.start()
        threadLatch.await()

        val logFile = File(tempDir, LOG_FILE_NAME)
        val content = logFile.readText()
        assertThat(
            content,
            CoreMatchers.allOf(
                CoreMatchers.containsString("Test 1"),
                CoreMatchers.containsString("Test 2")
            )
        )
        Timber.uproot(tree)
    }
}
