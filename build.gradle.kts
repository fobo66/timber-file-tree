// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.3.1" apply false
    id("com.android.library") version "8.3.1" apply false
    id("org.jetbrains.kotlin.android") version "1.9.23" apply false
    id("de.mannodermaus.android-junit5") version "1.10.0.0" apply false
    id("io.gitlab.arturbosch.detekt") version "1.23.6" apply false
}
